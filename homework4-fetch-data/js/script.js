class FilmsWithInfo {
        constructor(url) {
            this.url = url;
        }
        request() {
            const getResponse = new Promise((resolve, reject)=>{
                    const xhr = new XMLHttpRequest();
                    xhr.open("GET", this.url);
                    xhr.send();
                    xhr.onload = () => resolve(JSON.parse(xhr.response));
                    xhr.onerror = () => reject(e);
                });
            getResponse
                .then((response) => {
                    response.forEach(e => {
                        let name = e.name;
                        const li = document.createElement("li");
                        li.textContent = "Film name: " + name;
                        document.getElementById("films").append(li);
                        const titleFilmNumber = document.createElement("h3");
                        li.prepend(titleFilmNumber);
                        titleFilmNumber.textContent = "Film number: " + e.id;
                        const filmInfo = document.createElement("p");
                        filmInfo.textContent = "Film info: " + e.openingCrawl;
                        li.append(filmInfo);
                        const titlePers = document.createElement("h4");
                        titlePers.textContent = "Persons";
                        li.append(titlePers);
                        const allPerson = e.characters;
                        const newAllPerson = allPerson.forEach(e => {
                            axios
                                .get(e)
                                .then(function(response) {
                                    const namePers = document.createElement("li");
                                    const pers = document.createElement("ul");
                                    li.append(pers);
                                    pers.append(namePers);
                                    namePers.textContent = response.data.name;
                                });
                        });
                    });
                })
                .catch((error) => {
                    console.log(error);
                })
        }
}
    
const films = new FilmsWithInfo("https://ajax.test-danit.com/api/swapi/films/");
films.request();