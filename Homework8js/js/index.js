'use strict';
let myInput = document.getElementById('input');
let mySpan = document.getElementById('span-prise');
let myDiv = document.getElementById('div');

let button = document.createElement('button');
let spanToButton = document.createElement('span');
let spanAfterInput = document.createElement('span');

button.classList.add('button');
button.style.backgroundColor = 'green';
button.style.borderRadius = '50%';
button.style.cursor = 'pointer';
button.textContent = 'x';

myInput.addEventListener('focus', focusInput);
function focusInput() {
    myInput.style.outline = 'none';
    myInput.style.border = '3px solid green';
    myInput.style.backgroundColor = 'white';
    spanAfterInput.remove();
    button.remove();
    spanToButton.remove();
}

myInput.addEventListener('blur', blurInput);
function blurInput() {
    myInput.style.border = '1px solid black';
    if (myInput.value > 0) {
        myInput.style.backgroundColor = 'lightgreen';
        spanAfterInput.classList.add('span');
        spanToButton.textContent = `Текущая цена: ${myInput.value}`;
        myDiv.before(spanToButton);
        myDiv.before(button);
        button.addEventListener('click', deleteClickOnButton);
        function deleteClickOnButton() {
            myInput.style.backgroundColor = 'white';
            spanToButton.remove();
            button.remove();
            myInput.value = '';
        }
    } else {
        spanAfterInput.textContent = 'Please enter correct  price';
        myInput.style.outline = '2px dashed red';
        document.body.append(spanAfterInput);
    }
}