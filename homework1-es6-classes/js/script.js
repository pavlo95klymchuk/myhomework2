"use strict";

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        return this._name = value;
    }
    get age() {
        return this._age;
    }
    set age(value) {
        return this._age = value;
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        return this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this._lang = lang;
    }
        get lang() {
            return this._lang;
        }
        set lang(value) {
            return this._lang = value;
        }
        get salary() {
            return this._salary * 3;
        }
        set salary(value) {
            return this._salary = value;
        }
}

const programmerJava = new Programmer('Uasia', '22', '1000', 'Java');
const programmerJS = new Programmer('Petya', '24', '1200', 'JS');
const programmerPHP = new Programmer('Sereza', '28', '1600', 'PHP');

console.log(programmerJava);
console.log(programmerJS);
console.log(programmerPHP);