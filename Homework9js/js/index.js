'use strict';
let tabsTitle = document.querySelectorAll('.tabs-title');
let tabsItem = document.querySelectorAll('.tabs-item');
let tabs = document.querySelector('.tabs');

const classActive = 'active-item';
const classNoActive = 'no-active-item';

tabs.addEventListener('click', activeTitle);
function activeTitle(evt) {
    for (let i = 0; i < tabsTitle.length; i++) {
        if (tabsTitle[i] === evt.target) {
            evt.target.classList.add('active');
        } else if (tabsTitle[i] !== evt.target) {
            tabsTitle[i].classList.remove('active');
        }
    }
}

tabs.addEventListener('click', activeItem);
function activeItem(evt) {
    let addClassActiveOnItem = document.querySelector(`[data-item=${evt.target.id}]`);
    for (let i = 0; i < tabsItem.length; i++) {
        if (addClassActiveOnItem === tabsItem[i]) {
            addClassActiveOnItem.classList.remove(classNoActive);
            addClassActiveOnItem.classList.add(classActive);
        } else {
            tabsItem[i].classList.remove(classActive);
            tabsItem[i].classList.add(classNoActive);
        }
    }
}
    
