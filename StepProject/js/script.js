document.addEventListener('click', function (evt) {
    evt.preventDefault();
});


let ourServicesItems = document.querySelector('.our-services-items');

ourServicesItems.addEventListener('click', activeOurServices);
function activeOurServices(evt) {
    let ourServicesItem = document.querySelectorAll('.our-services-item');
    for (let i = 0; i < ourServicesItem.length; i++) {
        if (ourServicesItem[i] === evt.target) {
            evt.target.classList.add('our-services-item-active');
            evt.target.classList.add('our-services-item-click');
        } else if (ourServicesItem[i] !== evt.target) {
            ourServicesItem[i].classList.remove('our-services-item-active');
            ourServicesItem[i].classList.remove('our-services-item-click');
        }
    }
}

ourServicesItems.addEventListener('click', activeItemSingature);
function activeItemSingature(evt) {
    const classOurServicesActive = 'our-services-img-singature-active';
    const classOurServicesNoActive = 'our-services-img-singature-no-active';
    let addClassActiveOnItem = document.querySelector(`[data-our-services-item=${evt.target.id}]`);
    let ourServicesItemsImgSingature = document.querySelectorAll('.our-services-item-img-singature');
    for (let i = 0; i < ourServicesItemsImgSingature.length; i++) {
        if (addClassActiveOnItem === ourServicesItemsImgSingature[i]) {
            addClassActiveOnItem.classList.remove(classOurServicesNoActive);
            addClassActiveOnItem.classList.add(classOurServicesActive);
        } else {
            ourServicesItemsImgSingature[i].classList.remove(classOurServicesActive);
            ourServicesItemsImgSingature[i].classList.add(classOurServicesNoActive);
        }
    }
}



const amazingWorkButton = document.querySelector('.amazing-work-button');

amazingWorkButton.addEventListener('click', function() {
    amazingWorkButton.remove();
    const loader = document.querySelector('.loader');
    loader.style.cssText = 'display: flex;';
    setTimeout(() => {
        let amazingWorkItemNone = document.querySelectorAll('.amazing-work-item-none');
        amazingWorkItemNone.forEach(li => {
        if(li.classList.contains('amazing-work-item-none')) {
            li.classList.remove('amazing-work-item-none');
        }
        loader.remove();
    });
    }, 2000);
});

let amazingWorkItem = document.querySelectorAll('.amazing-work-item');
let amazingWorkMenuItem = document.querySelectorAll('.amazing-work-menu-item');
let amazingWorkMenu = document.querySelector('.amazing-work-menu');

amazingWorkMenu.addEventListener('click', function(evt) {
    amazingWorkItem.forEach((i) => {
        if (i.dataset.amazing === evt.target.id) {
            i.classList.add('display-block');
        }
        if (i.dataset.amazing !== evt.target.id) {
            i.classList.add('amazing-work-item-none');
            i.classList.remove('display-block');
        }
        if (evt.target.id === 'amazing-work-all') {
            i.classList.add('display-block');
        }
    });
});



let slideIndex = 1;
showSlides(slideIndex);

function plusSlide() {
    showSlides(slideIndex += 1);
}

function minusSlide() {
    showSlides(slideIndex -= 1);  
}

function currentxSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName('section-client-feedback-info-item');
    let dots = document.getElementsByClassName('client-feedback-active-user');
    if (n > slides.length) {
      slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" mn-top", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " mn-top";
}