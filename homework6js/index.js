'use strict';
let filterBy = (mass, type) => {
    let newMass = [];
    newMass = mass.filter(item => typeof item !== type);
    return newMass;
}
let myMass = ['hello', 'world', 23, '23', null];
console.log(filterBy(myMass, 'string'));