'use strict';
const buttonChangeTheTopic = document.getElementById('сhange-the-topic');
const body = document.querySelector('body');
let myTheme = localStorage.getItem('my-theme');
console.log(myTheme);

buttonChangeTheTopic.addEventListener('click', changeTopic);
function changeTopic() {
    if (localStorage.getItem('my-theme')) {
        body.classList.remove('theme');
        localStorage.removeItem('my-theme');
    } else {
        body.classList.add('theme');
        localStorage.setItem('my-theme', 'theme');
    }
}

window.onload = function () {
    if (localStorage.getItem('my-theme') !== null) {
        body.classList = myTheme;
        console.log(myTheme);
    }
}