const URL_IP = "https://api.ipify.org/?format=json";
const URL_GEOLOCATION = "http://ip-api.com/json/";
const btn = document.getElementById("btn");

class watchLocationOnIp{
    async myIp() {
        const requestForIp = await fetch(URL_IP);
        const myIpInObj = await requestForIp.json();
        const {ip} = myIpInObj;
        this.getGeolocation(ip);
    }
    async getGeolocation(ip) {
        const requestGeolocation = await fetch(URL_GEOLOCATION + ip + "?fields=status,continent,country,region,city,district");
        const myGeolocationInObj = await requestGeolocation.json();
        const ul = document.createElement("ul");
        const li = document.createElement("li")
        btn.after(ul);
        li.innerHTML = `<p>Status: ${myGeolocationInObj.status}</p><p>Continent: ${myGeolocationInObj.continent}</p><p>Country: ${myGeolocationInObj.country}</p><p>Region: ${myGeolocationInObj.region}</p><p>City: ${myGeolocationInObj.city}</p><p>District: ${myGeolocationInObj.district}</p>`;
        ul.append(li);
    }
}
const myLocation = new watchLocationOnIp();
btn.addEventListener("click", ()=>{
    myLocation.myIp();
});