const books = [
    { 
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70 
    }, 
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    }, 
    { 
        name: "Тысячекратная мысль",
        price: 70
    }, 
    { 
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    }, 
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const root = document.getElementById("root");
let listForBooks = document.createElement('ul');
root.prepend(listForBooks);

const newBooks = books.map(someBook => {
    const {author, name, price} = someBook;
    try {
        if (author && name && price) {
        return `<li>author: ${author} => name: ${name} => price: ${price}</li>`;
        } 
        else if (!author) {
            throw new Error(`author is ${author}`)
        }
        else if (!name) {
            throw new Error(`name is ${name}`)
        }
        else if (!price) {
            throw new Error(`price is ${price}`)
        }
    }
    catch(el) {
        console.error(el);
    }
    });

listForBooks.insertAdjacentHTML('beforeend', newBooks.join(""));