const headerBtn = document.getElementById("header-button");
const headerBtnActive = document.getElementById("header-button-active");
const headerBtnNoActive = document.getElementById("header-button-no-active");
const headerMenu = document.getElementById("header-menu");

headerBtn.addEventListener("click", clickOnHeaderBtn);
function clickOnHeaderBtn(evt) {
    headerBtnActive.classList.toggle("header__button-active");
    headerBtnActive.classList.toggle("header__button-no-active");
    headerBtnNoActive.classList.toggle("header__button-no-active");
    headerMenu.classList.toggle("header__menu-active");
}