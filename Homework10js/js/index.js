'use strict';
const letPassword = document.querySelector('.let-password');
const confirmPassword = document.querySelector('.confirm-password');

const iconEyeForLetPass = document.querySelector('.icon-eye-for-let-pass');
const iconCloseEyeForLetPass = document.querySelector('.icon-close-eye-for-let-pass')

const iconEyeForConfirmPass = document.querySelector('.icon-eye-for-confirm-pass');
const iconCloseEyeForConfirmPass = document.querySelector('.icon-close-eye-for-confirm-pass');

const btn = document.querySelector('.btn');
const spanError = document.createElement('span');

btn.addEventListener('click', clickBtn);
function clickBtn(evt) {
    evt.preventDefault();
    if(letPassword.value === confirmPassword.value && letPassword.value !== '') {
        alert('You are welcome');
    } else {
        btn.after(spanError);
        spanError.textContent = 'Нужно ввести одинаковые значения';
        spanError.style.color = 'red';
    }
}

iconEyeForLetPass.addEventListener ('click', clickOnEyeOnLetPassword);
function clickOnEyeOnLetPassword() {
    if (letPassword.getAttribute('type') === 'password') {
        letPassword.setAttribute('type', 'text');
        iconEyeForLetPass.classList.add('display-none');
        iconCloseEyeForLetPass.classList.add('display-block');
    } else {
        letPassword.setAttribute('type', 'password');
        iconCloseEyeForLetPass.classList.remove('display-block');
    }
}
iconCloseEyeForLetPass.addEventListener('click', clickOnCloseEyeOnLetPassword);
function clickOnCloseEyeOnLetPassword() {
    if (letPassword.getAttribute('type') === 'text') {
        letPassword.setAttribute('type', 'password');
        iconEyeForLetPass.classList.add('display-block');
        iconCloseEyeForLetPass.classList.remove('display-block');
        iconCloseEyeForLetPass.classList.add('display-none');
    } else {
        letPassword.setAttribute('type', 'text');
    }
}

iconEyeForConfirmPass.addEventListener ('click', clickOnEyeOnConfirmPassword);
function clickOnEyeOnConfirmPassword() {
    if (confirmPassword.getAttribute('type') === 'password') {
        confirmPassword.setAttribute('type', 'text');
        iconEyeForConfirmPass.classList.add('display-none');
        iconCloseEyeForConfirmPass.classList.add('display-block');
    } else {
        confirmPassword.setAttribute('type', 'password');
        iconCloseEyeForConfirmPass.classList.remove('display-block');
    }
}
iconCloseEyeForConfirmPass.addEventListener('click', clickOnCloseEyeOnconfirmPassword);
function clickOnCloseEyeOnconfirmPassword() {
    if (confirmPassword.getAttribute('type') === 'text') {
        confirmPassword.setAttribute('type', 'password');
        iconEyeForConfirmPass.classList.add('display-block');
        iconCloseEyeForConfirmPass.classList.remove('display-block');
        iconCloseEyeForConfirmPass.classList.add('display-none');
    } else {
        confirmPassword.setAttribute('type', 'text');
    }
}