let $goToUp = $('.go-to-up');

$(document).on('click', 'a[href^="#"]', function (evt) {
    evt.preventDefault();
    $('html, body').animate({
        scrollTop: $($(this).attr("href")).offset().top
    }, 600)
});

$goToUp.on('click', function () {
    $('html, body').animate({
        scrollTop: 0
    }, 1000)
});

$(document).on('scroll', function() {
    if (window.innerHeight < window.scrollY) {
        $goToUp.show();
    } else {
        $goToUp.hide();
    }
});

$('#header-hot-news').on('click', slideToggle);
function slideToggle() {
    $('.hot-news-all-posts').toggle();
}