'use strict';
const startBtn = document.getElementById('start');
const stopBtn = document.getElementById('stop');
const allPictures = document.querySelectorAll('.image-to-show');

let interval;
let numPicture = 0;

function showSlides() {
    interval = setInterval(() => {
        allPictures[numPicture].className = 'display-none';
        numPicture = (numPicture + 1) % allPictures.length;
        allPictures[numPicture].className = 'image-to-show';   
    }, 3000);
}
showSlides();

startBtn.addEventListener('click', function(){
    showSlides();
});

stopBtn.addEventListener('click', function(){
    clearInterval(interval);
});