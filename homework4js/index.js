'use strict';
function createNewUser() {
    let newUser = {
        firstName: '',
        lastName: '',
    };
    do {
        newUser.firstName = prompt('Your name:');
    } while (newUser.firstName.trim() === '' || !newUser.firstName || !isNaN(+newUser.firstName));
    do {
        newUser.lastName = prompt('Your second name:');
    } while (newUser.lastName.trim() === '' || !newUser.lastName || !isNaN(+newUser.lastName));
    newUser.getLogin = function () {
        let newLogin = newUser.firstName.charAt(0).toLowerCase() + newUser.lastName.toLowerCase();
        return newLogin;
    }
    console.log(`Your login is: ${newUser.getLogin()}`);
}
createNewUser();
